package com.pronetbeans.bamboo.planfavesdel;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.event.HibernateEventListener;
import com.atlassian.bamboo.event.PlanFavouriteAddedEvent;
import com.atlassian.bamboo.event.PlanFavouriteRemovedEvent;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.resultsummary.ResultsSummaryManager;
import com.atlassian.bamboo.security.StringEncrypter;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.event.Event;
import com.atlassian.spring.container.ContainerManager;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec2.binary.Base64;
import org.apache.log4j.Logger;

/**
 *
 * @author Adam Myat
 */
public class PlanFavouritesChangedListener implements HibernateEventListener {

    private static final Logger log = Logger.getLogger(PlanFavouritesChangedListener.class);
    private BandanaManager bandanaManager;
    private PlanManager planManager;
    private ResultsSummaryManager resultsSummaryManager;

    public AdministrationConfiguration getAdministrationConfiguration() {
        return (AdministrationConfiguration) ContainerManager.getComponent("administrationConfiguration");
    }

    @Override
    public Class[] getHandledEventClasses() {
        return new Class[]{
                    PlanFavouriteAddedEvent.class, PlanFavouriteRemovedEvent.class
                };
    }

    @Override
    public void handleEvent(Event event) {

        StringEncrypter encrypterRef = new StringEncrypter();
        
        if (event instanceof PlanFavouriteAddedEvent) {
            PlanFavouriteAddedEvent addedEvent = (PlanFavouriteAddedEvent) event;

            log.info("************ PlanFavouriteAddedEvent **************");
            String currentUser = addedEvent.getUsername();

            String encryptedUsername = (String) bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, "com.pronetbeans.bamboo.planfavesdel.username." + currentUser);
            String encryptedPassword = (String) bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, "com.pronetbeans.bamboo.planfavesdel.password." + currentUser);

            String decryptedUsername = encrypterRef.decrypt(encryptedUsername);
            String decryptedPassword = encrypterRef.decrypt(encryptedPassword);

            AdministrationConfiguration admin = getAdministrationConfiguration();

            String planKey = addedEvent.getPlanKey().getKey();

            String bambooPlanUrl = admin.getBaseUrl() + "/browse/" + planKey;

            URL url = null;
            HttpURLConnection connection = null;
            BufferedReader rd = null;
            OutputStreamWriter wr = null;
            try {
                String finalUrl = "http://api.del.icio.us/v1/posts/delete?url=" + bambooPlanUrl;
                log.info("finalUrl : " + finalUrl);
                url = new URL(finalUrl);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");

                Base64 enc = new Base64();
                String userpassword = decryptedUsername.trim() + ":" + decryptedPassword.trim();
                String encodedAuthorization = enc.encodeToString(userpassword.getBytes());

                connection.setRequestProperty("Authorization", "Basic " + encodedAuthorization);

                connection.setDoOutput(true);
                wr = new OutputStreamWriter(connection.getOutputStream());
                wr.write("");
                wr.flush();

                // Get the response
                rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                log.info("response data : ");
                while ((line = rd.readLine()) != null) {
                    log.info(line);
                }
                log.info("-----------------");
                int respCode = connection.getResponseCode();
                log.info("PlanFavouritesChangedListener : Response Code : " + respCode);

            } catch (Exception e) {

                e.printStackTrace();
            } finally {

                try {
                    wr.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (connection != null) {
                    connection.disconnect();
                }
            }

        } else if (event instanceof PlanFavouriteRemovedEvent) {
            PlanFavouriteRemovedEvent removedEvent = (PlanFavouriteRemovedEvent) event;

            String currentUser = removedEvent.getUsername();

            String encryptedUsername = (String) bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, "com.pronetbeans.bamboo.planfavesdel.username." + currentUser);
            String encryptedPassword = (String) bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, "com.pronetbeans.bamboo.planfavesdel.password." + currentUser);

            String decryptedUsername = encrypterRef.decrypt(encryptedUsername);
            String decryptedPassword = encrypterRef.decrypt(encryptedPassword);

            log.info("************ PlanFavouriteRemovedEvent **************");

            AdministrationConfiguration admin = getAdministrationConfiguration();

            String planKey = removedEvent.getPlanKey().getKey();

            String bambooPlanUrl = admin.getBaseUrl() + "/browse/" + planKey;
            
            URL url = null;
            HttpURLConnection connection = null;
            BufferedReader rd = null;
            OutputStreamWriter wr = null;
            try {
                String finalUrl = "http://api.del.icio.us/v1/posts/add?url=" + bambooPlanUrl + "&description=" + planKey + "&shared=no";
                log.info("finalUrl : " + finalUrl);
                url = new URL(finalUrl);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");

                Base64 enc = new Base64();
                String userpassword = decryptedUsername.trim() + ":" + decryptedPassword.trim();
                String encodedAuthorization = enc.encodeToString(userpassword.getBytes());

                connection.setRequestProperty("Authorization", "Basic " + encodedAuthorization);

                connection.setDoOutput(true);
                wr = new OutputStreamWriter(connection.getOutputStream());
                wr.write("");
                wr.flush();

                // Get the response
                rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                log.info("response data : ");
                while ((line = rd.readLine()) != null) {
                    log.info(line);
                }
                log.info("-----------------");
                int respCode = connection.getResponseCode();
                log.info("PlanFavouritesChangedListener : Response Code : " + respCode);

            } catch (Exception e) {

                e.printStackTrace();
            } finally {

                try {
                    wr.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (connection != null) {
                    connection.disconnect();
                }
            }
        }
    }

    /**
     * @return the bandanaManager
     */
    public BandanaManager getBandanaManager() {
        return bandanaManager;
    }

    /**
     * @param bandanaManager the bandanaManager to set
     */
    public void setBandanaManager(BandanaManager bandanaManager) {
        this.bandanaManager = bandanaManager;
    }

    /**
     * @return the planManager
     */
    public PlanManager getPlanManager() {
        return planManager;
    }

    /**
     * @param planManager the planManager to set
     */
    public void setPlanManager(PlanManager planManager) {
        this.planManager = planManager;
    }

    /**
     * @return the resultsSummaryManager
     */
    public ResultsSummaryManager getResultsSummaryManager() {
        return resultsSummaryManager;
    }

    /**
     * @param resultsSummaryManager the resultsSummaryManager to set
     */
    public void setResultsSummaryManager(ResultsSummaryManager resultsSummaryManager) {
        this.resultsSummaryManager = resultsSummaryManager;
    }
}
