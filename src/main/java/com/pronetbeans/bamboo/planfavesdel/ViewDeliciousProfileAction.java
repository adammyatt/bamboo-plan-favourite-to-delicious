package com.pronetbeans.bamboo.planfavesdel;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.security.StringEncrypter;
import com.atlassian.bamboo.user.BambooUser;
import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.atlassian.bamboo.ww2.actions.admin.user.UserAdministrationUtils;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.core.exception.InfrastructureException;
import com.atlassian.user.User;
import com.atlassian.util.concurrent.LazyReference;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Adam Myatt
 */
public class ViewDeliciousProfileAction extends BambooActionSupport {

    private String username;
    private String password;
    BambooUser currentUser;
    private String currentUserName;
    private BandanaManager bandanaManager;

    @Override
    public String doInput() throws Exception {

        if (isCurrentlyLoggedInUser()) {
            return doDefault();
        } else {
            if (getUser() == null) {
                // send to login screen instead
                return LOGIN;
            } else {
                addActionError("You do not have permission to edit the Delicious profile of this user");
                return ERROR;
            }
        }
    }

    @Override
    public String doDefault() throws Exception {
        if (isCurrentlyLoggedInUser() && getCurrentUser() != null) {

            final LazyReference<StringEncrypter> encrypterRef = new LazyReference<StringEncrypter>() {

                @Override
                protected StringEncrypter create() throws Exception {
                    return new StringEncrypter();
                }
            };

            String encryptedUsername = (String) bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, "com.pronetbeans.bamboo.planfavesdel.username." + getCurrentUser().getUsername());
            String encryptedPassword = (String) bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, "com.pronetbeans.bamboo.planfavesdel.password." + getCurrentUser().getUsername());
            String decryptedUsername = encrypterRef.get().decrypt(encryptedUsername);
            String decryptedPassword = encrypterRef.get().decrypt(encryptedPassword);

            this.username = decryptedUsername;
            this.password = decryptedPassword;

            return INPUT;
        } else {
            if (getUser() == null) {
                // send to login screen instead
                return LOGIN;
            } else {
                addActionError("You do not have permission to view the Delicious profile of this user");
                return ERROR;
            }
        }

    }

    @Override
    public void validate() {
//        if (this.username == null || this.password==null)
//        {
//            addFieldError("username", getText("ideConnector.port.error.invalid"));
//        }
    }

    @Override
    public String execute() throws Exception {

        if (!isCurrentlyLoggedInUser()) {
            addActionError("You do not have permission to edit the Delicious profile of this user");
            return ERROR;
        }

        if (getCurrentUser() != null) {
            try {
                // save username and password field ? to Bandana ?
                final LazyReference<StringEncrypter> encrypterRef = new LazyReference<StringEncrypter>() {

                    @Override
                    protected StringEncrypter create() throws Exception {
                        return new StringEncrypter();
                    }
                };

                String encryptedUsername = encrypterRef.get().encrypt(username);
                String encryptedPassword = encrypterRef.get().encrypt(password);

                bandanaManager.setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, "com.pronetbeans.bamboo.planfavesdel.username." + getCurrentUser().getUsername(), encryptedUsername);
                bandanaManager.setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, "com.pronetbeans.bamboo.planfavesdel.password." + getCurrentUser().getUsername(), encryptedPassword);

                return SUCCESS;
            } catch (InfrastructureException e) {
                UserAdministrationUtils.processInfrastructureExceptionForCrowdException(e, this);
                return ERROR;
            }
        } else {
            addActionError(getText("user.admin.edit.failed", new String[]{currentUserName}));
            return ERROR;
        }
    }

    public boolean isCurrentlyLoggedInUser() {
        if (getUser() == null || getCurrentUser() == null || getCurrentUser().getUser() == null) {
            return false;
        }

        return getCurrentUser().getUser().equals(getUser());
    }

    public BambooUser getCurrentUser() {
        if (currentUser == null) {
            if (StringUtils.isBlank(currentUserName)) {
                User user = getUser();
                if (user != null) {
                    currentUser = getBambooUserManager().getBambooUser(user.getName());
                }
            } else {
                currentUser = getBambooUserManager().getBambooUser(currentUserName);
            }
        }

        return currentUser;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the bandanaManager
     */
    public BandanaManager getBandanaManager() {
        return bandanaManager;
    }

    /**
     * @param bandanaManager the bandanaManager to set
     */
    public void setBandanaManager(BandanaManager bandanaManager) {
        this.bandanaManager = bandanaManager;
    }
}
