[#-- @ftlvariable name="action" type="com.pronetbeans.bamboo.planfavesdel.ViewDeliciousProfileAction" --]
[#-- @ftlvariable name="" type="com.pronetbeans.bamboo.planfavesdel.ViewDeliciousProfileAction" --]

<html>
<head>
    [@ui.header pageKey="User Profile - Delicious Account" title=true /]
    <meta name="decorator" content="atl.userprofile">
    <meta name="tab" content="delicious">
</head>
<body>

 [@ww.form id='configureDeliciousFavesPlugin' action='updateDeliciousAccount.action' submitLabelKey='Save Configuration' titleKey='Delicious Account Credentials']

<br>

[@ui.bambooSection]
   [@ww.textfield labelKey="Username : " toggle='true' name='username'/]<br>
   [@ww.password labelKey='Password : ' name='password' showPassword="true" /]
[/@ui.bambooSection]

        [@ui.clear /]

    [/@ww.form]
<br>
<br>

</body>
</html>